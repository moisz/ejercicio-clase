﻿using System;

namespace _2
{
    
    class Cliente
    {
        private string nombre;
        private int total_cliente;

        public Cliente(string n)
        {
            nombre = n;
            total_cliente = 0;
        }

        public void Depositar(int depo)
        {
            total_cliente = total_cliente + depo;
        }

        public void Retirar(int reti)
        {
            total_cliente = total_cliente - reti;
        }

        public int Retornar()
        {
            return total_cliente;
        }

        public void Mostrar()
        {
            Console.WriteLine("{0} tiene en su cuenta un total de: {1}", nombre, total_cliente + "\n");
        }
    }

    class Banco
    {
        private Cliente Cliente_no_1, Cliente_no_2, Cliente_no_3; 

        public Banco() 
        {
            Cliente_no_1 = new Cliente("Jesus Polanco");
            Cliente_no_2 = new Cliente("Maria Altagracia");
            Cliente_no_3 = new Cliente("Jose Luis"); 
        }

        public void Movimiento()
        {
            Cliente_no_1.Depositar(4500);
            Cliente_no_2.Depositar(15000);
            Cliente_no_3.Depositar(73280);

            Cliente_no_1.Retirar(1500);
            Cliente_no_3.Retirar(50500);
        }

        public void DepositosTotales()
        {
            int totalbanco = Cliente_no_1.Retornar () + Cliente_no_2.Retornar () + Cliente_no_3.Retornar ();

            Console.WriteLine ("\n" + "Total de dinero depositado en el banco: " + totalbanco + "\n");
            
            Cliente_no_1.Mostrar();
            Cliente_no_2.Mostrar();
            Cliente_no_3.Mostrar();
            Console.ReadKey();
        }
    }
    class Inicializador
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("-------------------------------- Banco Central --------------------------------");
            Banco banco_central = new Banco();
            banco_central.Movimiento();
            banco_central.DepositosTotales();
            
        }
    }
}
